core = 7.x
api = 2

; uw_ct_teaching_tip
projects[uw_ct_teaching_tip][type] = "module"
projects[uw_ct_teaching_tip][download][type] = "git"
projects[uw_ct_teaching_tip][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_teaching_tip.git"
projects[uw_ct_teaching_tip][download][tag] = "7.x-1.2"
projects[uw_ct_teaching_tip][subdir] = ""

